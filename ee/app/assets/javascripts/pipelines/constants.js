export default {
  triggeredPipelines: 'triggeredPipelines',
  triggeredByPipelines: 'triggeredByPipelines',
  triggeredBy: 'triggeredBy',
  triggered: 'triggered',
};
